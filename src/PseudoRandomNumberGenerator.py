#! /usr/bin/env python3
# coding: utf8
from time import time

class PseudoRandomNumberGenerator:
    def __init__(self, k=None, a=None, b=None, m=None):
        self.k = int(time()) if k is None else int(k)
        self.a = 1103515245 if a is None else int(a)
        self.b = 12345 if b is None else int(b)
        self.m = 31 if m is None else int(m)

    def rand(self):
        seed = self.a * self.k + self.b
        self.k = seed
        return seed % 2**self.m

    def rand_float(self):
        return self.rand() / float(2**self.m)
