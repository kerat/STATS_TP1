# /usr/bin/env python3
# coding: utf8
from PseudoRandomNumberGenerator import PseudoRandomNumberGenerator


def question1():
    print("Voici 10 nombres pseudo-aléatoires générés avec les paramètres :\n"
          "a = 1103515245\n"
          "b = 12345\n"
          "m = 32768\n"
          "Seed = timestamp\n")
    rand1 = PseudoRandomNumberGenerator()
    for i in range(10):
        print(rand1.rand())


def question2():
    n = 0
    a, b, m, k = 6, 2, 24, 5
    print("Voici " + str(n) +
    " nombres pseudo-aléatoires générés"
    " avec les paramètres :\n"
    "a = " + str(a) + "\n"
    "b = " + str(b) + "\n"
    "m = " + str(m) + "\n"
    "Seed = " + str(k))
    rand1 = PseudoRandomNumberGenerator(k, a, b, m)
    liste = []
    while True:
        r = rand1.rand()
        if r in liste:
            print(r)
            break
        else:
            liste.append(r)
            n = n + 1
            print(r)
    print("On boucle au bout de " + str(n) + " fois.")


def question3_1():
    n = 1000
    bm = None
    b = None
    print("Voici " + str(n) + " nombres pseudo-aléatoires générés "
          "avec les paramètres :\n"
          "a = 1103515245\n"
          "b = 12345\n"
          "m = 32768\n"
          "Seed = timestamp\n")
    rand1 = PseudoRandomNumberGenerator()
    for i in range(n):
        if i != 0:
            bm = b
        b = rand1.rand() % 2
        print(b)
        if b == bm:
            print("Le bit de poids faible du nombre est le même que le bit de poids faible que le nombre précédent. n = " + str(n))
            break

def question3_2():
    n = 1000
    bm = None
    b = None
    a, b, m, k = 6, 2, 24, 5
    print("Voici " + str(n) +
          " nombres pseudo-aléatoires générés"
          " avec les paramètres :\n"
          "a = " + str(a) + "\n"
          "b = " + str(b) + "\n"
          "m = " + str(m) + "\n"
          "Seed = " + str(k))
    rand1 = PseudoRandomNumberGenerator()
    for i in range(n):
        if i != 0:
            bm = b
        b = rand1.rand() % 2
        print(b)
        if b == bm:
            print("Le bit de poids faible du nombre est le même que le bit de poids faible que le nombre précédent. n = " + str(n))
            break


if __name__ == '__main__':
    question3_2()
