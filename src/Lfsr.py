#! /usr/bin/env python3
# coding: utf8

class Lfsr:
    def __init__(self, seed, size=32):
        if size < 32:
            self.size = 32
        else:
            self.size = size
        self.seed = seed
        self.bits = [int(b) for b in ('{0:0'+str(self.size)+'b}').format(self.seed)]

    def shift(self):
        b = self.bits[self.size-1] ^ self.bits[self.size-4] ^ self.bits[self.size-6] ^ self.bits[self.size-30]
        for i in reversed(range(self.size)):
            if i == 0:
                self.bits[i] = 0
            self.bits[i] = self.bits[i-1]
        self.bits[0] = b

    def new_int(self):
        for i in range(32):
            self.shift()
        return int(self.__str__(), 2)

    def __str__(self):
        return ''.join([str(b) for b in self.bits])
