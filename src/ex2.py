#! /usr/bin/env python3 
# coding: utf8
from Lfsr_8bits import Lfsr8

def question1():
    lfsr = Lfsr8()
    print(lfsr)
    print(lfsr.rand())
    print(lfsr.rand())
    print(lfsr.rand())
    print(lfsr.rand())
    
if __name__ == '__main__':
    question1()
