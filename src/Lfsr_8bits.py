#! /usr/bin/env python3
# coding: utf8
from time import time

class Lfsr8:
    ''' Classe du générateur de nombre aléatoire de type
    registre à décalage à rétroaction linéaire de 8 bits.
    S'iniatilise avec une graine sinon utilise le nombre
    de secondes écoulées depuis le 1er janvier 1970 comme
    graine.
    Le nombre est stocké sur 1 octet.
    '''
    def __init__(self, seed=None):
        ''' Constructeur du lfsr
        Prends en argument une graine ou rien du tout.
        Si la graine fournie est égale à 0 alors elle
        est ignorée.
        Retourne un objet Lfsr8
        '''
        self.bits = bytes()
        if seed == None or seed == 0:
            self.bits = (int(time()) & 255)
        else:
            self.bits = seed & 255
    
    def shift(self):
        ''' La methode shift genere un nouveau bit
        à partir des bits déjà générés.
        Le nouveau bit généré vérifie la formule :
        nouveau_bit = b0 ^ b3 ^ b5
        Enfin la méthode effectue un décalage à droite
        et le nouveau bit deviens le bit de poids fort.
        '''
        b_5 = (self.bits & 32) << 2
        b_3 = (self.bits & 8) << 4
        b_0 = (self.bits & 1) << 7
        bn = b_3 ^ b_5 ^ b_8
        self.bits = self.bits >> 1
        self.bits = self.bits | bn

    def get_rand(self):
        ''' Cette méthode renvoie simplement le
        nombre porté par l'objet sous la forme d'un
        flottant compris entre 0 (compris) et 1 (exclu).
        '''
        return ((self.bits & 1) +
                    (self.bits & 2) +
                    (self.bits & 4) +
                    (self.bits & 8) +
                    (self.bits & 16) +
                    (self.bits & 32) +
                    (self.bits & 64) +
                    (self.bits & 128))/256
    
    def rand(self):
        ''' Cette méthode est une méta-méthode qui
        effectue un décalage (méthode shift()), puis renvoie
        le nombre porté par l'objet sous la forme d'un flottant
        compris entre 0 (compris) et 1 (exclu).
        '''
        self.shift()
        return self.get_rand()

    def __str__(self):
        ''' Affiche le nombre porté par l'objet
        sous forme binaire (sans préfixe 0b et
        sans zéros non-significatifs
        '''
        return bin(self.bits).replace('0b','')

class Lfsr8_bis:
    ''' Similaire à Lfsr8 mais avec un shift différent'''
    def __init__(self, seed=None):
        ''' Constructeur du lfsr
        Prends en argument une graine ou rien du tout.
        Si la graine fournie est égale à 0 alors elle
        est ignorée.
        Retourne un objet Lfsr8
        '''
        self.bits = bytes()
        if seed == None or seed == 0:
            self.bits = (int(time()) & 255)
        else:
            self.bits = seed & 255
    
    def shift(self):
        ''' La methode shift genere un nouveau bit
        à partir des bits déjà générés.
        Le nouveau bit généré vérifie la formule :
        nouveau_bit = b1 ^ b3 ^ b4 ^ b7
        Enfin la méthode effectue un décalage à droite
        et le nouveau bit deviens le bit de poids fort.
        '''
        b_1 = (self.bits & 2) << 6
        b_3 = (self.bits & 8) << 4
        b_4 = (self.bits & 16) << 3
        b_7 = (self.bits & 128) 
        bn = b_4 ^ b_3 ^ b_4 ^ b_7
        self.bits = self.bits >> 1
        self.bits = self.bits | bn

    def get_rand(self):
        ''' Cette méthode renvoie simplement le
        nombre porté par l'objet sous la forme d'un
        flottant compris entre 0 (compris) et 1 (exclu).
        '''
        return ((self.bits & 1) +
                    (self.bits & 2) +
                    (self.bits & 4) +
                    (self.bits & 8) +
                    (self.bits & 16) +
                    (self.bits & 32) +
                    (self.bits & 64) +
                    (self.bits & 128))/256
    
    def rand(self):
        ''' Cette méthode est une méta-méthode qui
        effectue un décalage (méthode shift()), puis renvoie
        le nombre porté par l'objet sous la forme d'un flottant
        compris entre 0 (compris) et 1 (exclu).
        '''
        self.shift()
        return self.get_rand()

    def __str__(self):
        ''' Affiche le nombre porté par l'objet
        sous forme binaire (sans préfixe 0b et
        sans zéros non-significatifs
        '''
        return bin(self.bits).replace('0b','')
