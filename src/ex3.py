#! /usr/bin/env python3
# coding: utf8

from random import Random
from PseudoRandomNumberGenerator import PseudoRandomNumberGenerator
from Lfsr_8bits import Lfsr8
from Lfsr_8bits import Lfsr8_bis
import matplotlib.pyplot as plt

def question1_pyrandom():
    ''' Utilise le generateur de Python '''
    plt.figure(1)
    random = Random()
    valeurs = []
    for i in range(1000):
        valeurs.append(random.random())
    plt.plot(range(1000), valeurs, 'r+')
    plt.savefig('pyrandom_plot.png')

def question1_prng():
    ''' Utilise le generateur de la partie 1 '''
    plt.figure(2)
    prng = PseudoRandomNumberGenerator()
    valeurs = []
    for i in range(1000):
        valeurs.append(prng.rand_float())
    plt.plot(range(1000), valeurs, 'r+')
    plt.savefig('prng_plot.png')

def question1_lfsr():
    ''' Utilise le generateur à registre à rétroaction à décalage linéaire '''
    plt.figure(3)
    lfsr = Lfsr8()
    valeurs = []
    for i in range(1000):
        valeurs.append(lfsr.rand())
    plt.plot(range(1000), valeurs, 'r+')
    plt.savefig('lfsr_plot.png')

def question3_prng():
    ''' Utilise le generateur de la partie 1 '''
    plt.figure(4)
    prng = PseudoRandomNumberGenerator(5,6,2,24)
    valeurs = []
    for i in range(1000):
        valeurs.append(prng.rand_float())
    plt.plot(range(1000), valeurs, 'r+')
    plt.savefig('prng_plot.png')

def question3_lfsr():
    ''' Utilise le generateur à registre à rétroaction à décalage linéaire '''
    plt.figure(5)
    lfsr = Lfsr8_bis()
    valeurs = []
    for i in range(1000):
        valeurs.append(lfsr.rand())
    plt.plot(range(1000), valeurs, 'r+')
    plt.savefig('lfsr_plot.png')

if __name__ == '__main__':
#    question1_pyrandom()
#    question1_prng()
#    question1_lfsr()
#    question3_prng()
    question3_lfsr()
